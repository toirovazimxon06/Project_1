package uz.itpu;

import itpu.uz.controller.factory.FurnitureDaoFactory;
import itpu.uz.controller.factory.FurnitureDaoFactoryImpl;
import itpu.uz.dao.*;
import itpu.uz.entity.Bed;
import itpu.uz.entity.Mattress;
import itpu.uz.entity.Sofa;
import itpu.uz.entity.Table;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FurnitureDaoFactoryImplTest {

    private final FurnitureDaoFactory factory = new FurnitureDaoFactoryImpl();

    @Test
    public void testGetTableDao() {
        FurnitureDao<Table> dao = factory.getFurnitureDAO(Table.class);
        assertNotNull(dao);
        assertTrue(dao instanceof TableDao);
    }

    @Test
    public void testGetBedDao() {
        FurnitureDao<Bed> dao = factory.getFurnitureDAO(Bed.class);
        assertNotNull(dao);
        assertTrue(dao instanceof BedDao);
    }

    @Test
    public void testGetMattressDao() {
        FurnitureDao<Mattress> dao = factory.getFurnitureDAO(Mattress.class);
        assertNotNull(dao);
        assertTrue(dao instanceof MattressDao);
    }

    @Test
    public void testGetSofaDao() {
        FurnitureDao<Sofa> dao = factory.getFurnitureDAO(Sofa.class);
        assertNotNull(dao);
        assertTrue(dao instanceof SofaDao);
    }

    @Test
    public void testGetUnknownDao() {
        FurnitureDao<?> dao = factory.getFurnitureDAO(null);
        assertNull(dao);
    }

}

