package uz.itpu;

import itpu.uz.dao.BedDao;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ExceptionTest {
    @Test
    public void testParseValuesWithInvalidData() {
        String csvLineWithMissingData = "Bed,King Size,500.0,ten,80.0"; // 'ten' is invalid for quantity
        String[] valuesWithMissingData = csvLineWithMissingData.split(",");

        BedDao bedDao = new BedDao("Beds.csv");

        // Assert that a NumberFormatException is thrown because "ten" cannot be parsed as an integer
        assertThrows(NumberFormatException.class, () -> {
            bedDao.parseValues(valuesWithMissingData);
        });
    }

}
