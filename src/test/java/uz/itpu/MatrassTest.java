package uz.itpu;

import itpu.uz.controller.criteria.ComfortSearch;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.dao.MattressDao;
import itpu.uz.entity.Mattress;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

public class MatrassTest {
    MattressDao dao = new MattressDao("Mattresses_Test.csv");

    @Test
    void shouldFindAll() {

//        Mattress mattress1 = new Mattress();
//        mattress1.setFurnitureType("Mattress");
//        mattress1.setFurnitureName("Ortho Bliss Mattress");
//        mattress1.setPrice(750);
//        mattress1.setQuantity(8);
//        mattress1.setLength(100);
//        mattress1.setComfortLevel("Firm");

        Mattress mattress2 = new Mattress();
        mattress2.setFurnitureType("Mattress");
        mattress2.setFurnitureName("Cloud Comfort Mattress");
        mattress2.setPrice(850);
        mattress2.setQuantity(15);
        mattress2.setLength(80);
        mattress2.setComfortLevel("Soft");

        Mattress mattress3 = new Mattress();
        mattress3.setFurnitureType("Mattress");
        mattress3.setFurnitureName("Plush Airflow Mattress");
        mattress3.setPrice(950);
        mattress3.setQuantity(12);
        mattress3.setLength(150);
        mattress3.setComfortLevel("Medium-Firm");

        Mattress mattress4 = new Mattress();
        mattress4.setFurnitureType("Mattress");
        mattress4.setFurnitureName("Aqua Luxe Mattress");
        mattress4.setPrice(1050);
        mattress4.setQuantity(10);
        mattress4.setLength(120);
        mattress4.setComfortLevel("Medium-Soft");

        Mattress mattress5 = new Mattress();
        mattress5.setFurnitureType("Mattress");
        mattress5.setFurnitureName("Ergo Support Mattress");
        mattress5.setPrice(1150);
        mattress5.setQuantity(8);
        mattress5.setLength(100);
        mattress5.setComfortLevel("Firm");

        Mattress mattress6 = new Mattress();
        mattress6.setFurnitureType("Mattress");
        mattress6.setFurnitureName("Dreamy Bunk Bed Mattress");
        mattress6.setPrice(1250);
        mattress6.setQuantity(15);
        mattress6.setLength(80);
        mattress6.setComfortLevel("Soft");


        List<Mattress> expected = Arrays.asList(mattress2, mattress3,
                mattress4, mattress5, mattress6);

        SearchProperty<Mattress> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Mattress.class))).thenReturn(true);
        Iterable<Mattress> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);
    }

    @Test
    void shouldFindNone() {

        SearchProperty<Mattress> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Mattress.class))).thenReturn(false);
        Iterable<Mattress> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        List<Mattress> expected = Collections.emptyList();
        assertIterableEquals(expected, iterable);
    }

    @Test
    void shouldFindSome(){
        Mattress mattress2 = new Mattress();
        mattress2.setFurnitureType("Mattress");
        mattress2.setFurnitureName("Cloud Comfort Mattress");
        mattress2.setPrice(850);
        mattress2.setQuantity(15);
        mattress2.setLength(80);
        mattress2.setComfortLevel("Soft");

        List<Mattress> expected = Arrays.asList(mattress2);

        SearchProperty<Mattress> se = new ComfortSearch();
        se.addParameter(f-> f.getPrice()==850);
        Iterable<Mattress> iterable = dao.find(se);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);

    }

}
