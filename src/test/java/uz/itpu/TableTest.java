package uz.itpu;

import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.controller.criteria.TableSearch;
import itpu.uz.dao.TableDao;
import itpu.uz.entity.Table;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

public class TableTest {
    TableDao dao = new TableDao("Tables_Test.csv");

    @Test
    void shouldFindAll() {
//
//        Table table1 = new Table();
//        table1.setFurnitureType("Table");
//        table1.setFurnitureName("Family Dining Table");
//        table1.setPrice(220);
//        table1.setQuantity(10);
//        table1.setLength(200);
//        table1.setNumberOfPeople(8);

        Table table2 = new Table();
        table2.setFurnitureType("Table");
        table2.setFurnitureName("Centerpiece Coffee Table");
        table2.setPrice(60);
        table2.setQuantity(25);
        table2.setLength(80);
        table2.setNumberOfPeople(2);

        Table table3 = new Table();
        table3.setFurnitureType("Table");
        table3.setFurnitureName("Professional Work Desk");
        table3.setPrice(110);
        table3.setQuantity(8);
        table3.setLength(120);
        table3.setNumberOfPeople(4);

        Table table4 = new Table();
        table4.setFurnitureType("Table");
        table4.setFurnitureName("Nightstand");
        table4.setPrice(90);
        table4.setQuantity(12);
        table4.setLength(100);
        table4.setNumberOfPeople(4);

        Table table5 = new Table();
        table5.setFurnitureType("Table");
        table5.setFurnitureName("Vanity Table");
        table5.setPrice(140);
        table5.setQuantity(3);
        table5.setLength(140);
        table5.setNumberOfPeople(6);

        Table table6 = new Table();
        table6.setFurnitureType("Table");
        table6.setFurnitureName("Elegant Dining Table");
        table6.setPrice(170);
        table6.setQuantity(18);
        table6.setLength(180);
        table6.setNumberOfPeople(8);


        List<Table> expected = Arrays.asList(table2, table3, table4, table5, table6);

        SearchProperty<Table> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Table.class))).thenReturn(true);
        Iterable<Table> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);
    }

    @Test
    void shouldFindNone() {
        SearchProperty<Table> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Table.class))).thenReturn(false);
        Iterable<Table> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        List<Table> expected = Collections.emptyList();
        assertIterableEquals(expected, iterable);
    }
    @Test
    void shouldFindSome(){
        Table table2 = new Table();
        table2.setFurnitureType("Table");
        table2.setFurnitureName("Centerpiece Coffee Table");
        table2.setPrice(60);
        table2.setQuantity(25);
        table2.setLength(80);
        table2.setNumberOfPeople(2);
        List<Table> expected = Arrays.asList(table2);

        SearchProperty<Table> se = new TableSearch();
        se.addParameter(f-> f.getPrice()==60);
        Iterable<Table> iterable = dao.find(se);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);

    }
}
