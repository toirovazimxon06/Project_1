package uz.itpu;

import itpu.uz.controller.criteria.BedSearch;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.dao.BedDao;
import itpu.uz.entity.Bed;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

public class BedTest {
    BedDao dao = new BedDao("Beds_Test.csv");

    @Test
    void shouldFindAll() {

        Bed bed1 = new Bed();
        bed1.setFurnitureType("Bed");
        bed1.setFurnitureName("Bunk Bed");
        bed1.setPrice(350);
        bed1.setQuantity(15);
        bed1.setLength(190);
        bed1.setNumberOfDrawers(3);

        Bed bed2 = new Bed();
        bed2.setFurnitureType("Bed");
        bed2.setFurnitureName("King Size Canopy Bed");
        bed2.setPrice(600);
        bed2.setQuantity(25);
        bed2.setLength(200);
        bed2.setNumberOfDrawers(1);

        Bed bed3 = new Bed();
        bed3.setFurnitureType("Bed");
        bed3.setFurnitureName("Daydreamer's Loft Bed");
        bed3.setPrice(450);
        bed3.setQuantity(12);
        bed3.setLength(200);
        bed3.setNumberOfDrawers(4);

        Bed bed4 = new Bed();
        bed4.setFurnitureType("Bed");
        bed4.setFurnitureName("Sleeper Sofa Bed");
        bed4.setPrice(320);
        bed4.setQuantity(18);
        bed4.setLength(185);
        bed4.setNumberOfDrawers(2);

        Bed bed5 = new Bed();
        bed5.setFurnitureType("Bed");
        bed5.setFurnitureName("Rollaway Guest Bed");
        bed5.setPrice(280);
        bed5.setQuantity(10);
        bed5.setLength(190);
        bed5.setNumberOfDrawers(3);


        List<Bed> expected = Arrays.asList( bed1,bed2, bed3, bed4, bed5);

        SearchProperty<Bed> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Bed.class))).thenReturn(true);
        Iterable<Bed> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);
    }

    @Test
    void shouldFindSome(){
        Bed bed1 = new Bed();
        bed1.setFurnitureType("Bed");
        bed1.setFurnitureName("Bunk Bed");
        bed1.setPrice(350);
        bed1.setQuantity(15);
        bed1.setLength(190);
        bed1.setNumberOfDrawers(3);

        List<Bed> expected = Arrays.asList(bed1);

        SearchProperty<Bed> se = new BedSearch();
        se.addParameter(f-> f.getPrice()==350);
        Iterable<Bed> iterable = dao.find(se);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);

    }

    @Test
    void shouldFindNone() {

        SearchProperty<Bed> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Bed.class))).thenReturn(false);
        Iterable<Bed> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        List<Bed> expected = Collections.emptyList();
        assertIterableEquals(expected, iterable);
    }



}

