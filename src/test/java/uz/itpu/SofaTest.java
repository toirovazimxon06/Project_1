package uz.itpu;

import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.controller.criteria.SofaSearch;
import itpu.uz.dao.SofaDao;
import itpu.uz.entity.Sofa;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

public class SofaTest {
    SofaDao dao = new SofaDao("Sofas_Test.csv");

    @Test
    void shouldFindAll() {

//        Sofa sofa1 = new Sofa();
//        sofa1.setFurnitureType("Sofa");
//        sofa1.setFurnitureName("Convertible Sleeper Sofa");
//        sofa1.setPrice(900);
//        sofa1.setQuantity(8);
//        sofa1.setLength(190);
//        sofa1.setNumberOfSeats(3);

        Sofa sofa2 = new Sofa();
        sofa2.setFurnitureType("Sofa");
        sofa2.setFurnitureName("Cozy Lounge Chair");
        sofa2.setPrice(350);
        sofa2.setQuantity(8);
        sofa2.setLength(100);
        sofa2.setNumberOfSeats(1);

        Sofa sofa3 = new Sofa();
        sofa3.setFurnitureType("Sofa");
        sofa3.setFurnitureName("Relaxation Recliner");
        sofa3.setPrice(450);
        sofa3.setQuantity(3);
        sofa3.setLength(100);
        sofa3.setNumberOfSeats(1);

        Sofa sofa4 = new Sofa();
        sofa4.setFurnitureType("Sofa");
        sofa4.setFurnitureName("Luxe Chaise Lounge");
        sofa4.setPrice(550);
        sofa4.setQuantity(3);
        sofa4.setLength(150);
        sofa4.setNumberOfSeats(2);

        Sofa sofa5 = new Sofa();
        sofa5.setFurnitureType("Sofa");
        sofa5.setFurnitureName("Plush Bean Bag Chair");
        sofa5.setPrice(250);
        sofa5.setQuantity(8);
        sofa5.setLength(100);
        sofa5.setNumberOfSeats(1);

        List<Sofa> expected = Arrays.asList(sofa2, sofa3, sofa4, sofa5);

        SearchProperty<Sofa> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Sofa.class))).thenReturn(true);
        Iterable<Sofa> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);
    }

    @Test
    void shouldFindNone() {

        SearchProperty<Sofa> mockProperty = Mockito.mock(SearchProperty.class);
        when(mockProperty.isValid(Mockito.any(Sofa.class))).thenReturn(false);
        Iterable<Sofa> iterable = dao.find(mockProperty);

        assertNotNull(iterable);
        List<Sofa> expected = Collections.emptyList();
        assertIterableEquals(expected, iterable);
    }

    @Test
    void shouldFindSome(){
        Sofa sofa1 = new Sofa();
        sofa1.setFurnitureType("Sofa");
        sofa1.setFurnitureName("Convertible Sleeper Sofa");
        sofa1.setPrice(900);
        sofa1.setQuantity(8);
        sofa1.setLength(190);
        sofa1.setNumberOfSeats(3);
        List<Sofa> expected = Arrays.asList(sofa1);

        SearchProperty<Sofa> se = new SofaSearch();
        se.addParameter(f-> f.getPrice()==850);
        Iterable<Sofa> iterable = dao.find(se);

        assertNotNull(iterable);
        assertIterableEquals(expected, iterable);

    }
}
