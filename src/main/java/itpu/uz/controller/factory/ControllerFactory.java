package itpu.uz.controller.factory;


import itpu.uz.controller.control.*;
import itpu.uz.service.ApplianceService;
import itpu.uz.view.View;
import itpu.uz.view.WarehouseView;

import java.util.Map;

public class ControllerFactory {

    public WarehouseView createController(ApplianceService service) {
        View view = new View();

        TableFactory tableControllerFactory = new TableFactory();
        BedFactory bedControllerFactory = new BedFactory();
        MattressFactory mattressControllerFactory = new MattressFactory();
        SofaFactory sofaControllerFactory = new SofaFactory();

        TableCon tableController = tableControllerFactory.createTableController(service);
        BedCon bedController = bedControllerFactory.createBedController(service);
        MattressCon mattressController = mattressControllerFactory.createMattressController(service);
        SofaCon sofaController = sofaControllerFactory.createSofaController(service);

        Map<String, FurnitureCon<?>> furnitureControllers = Map.of(
                "tables", tableController,
                "beds", bedController,
                "mattresses", mattressController,
                "sofas", sofaController
        );

        return new WarehouseView(furnitureControllers, view);
    }

}
