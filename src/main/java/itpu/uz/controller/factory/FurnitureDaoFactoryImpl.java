package itpu.uz.controller.factory;


import itpu.uz.dao.*;
import itpu.uz.entity.Furniture;
import itpu.uz.entity.Mattress;
import itpu.uz.entity.Sofa;
import itpu.uz.entity.Table;
import itpu.uz.entity.Bed;
@SuppressWarnings("unchecked")
public class FurnitureDaoFactoryImpl implements FurnitureDaoFactory {

    @Override
    public <F extends Furniture<F>> FurnitureDao<F> getFurnitureDAO(Class<F> furnitureClass) {
        if (Table.class.equals(furnitureClass)) {
            return (FurnitureDao<F>) new TableDao("Tables.csv");
        }
        if (Bed.class.equals(furnitureClass)) {
            return (FurnitureDao<F>) new BedDao("Beds.csv");
        }
        if (Mattress.class.equals(furnitureClass)) {
            return (FurnitureDao<F>) new MattressDao("Mattresses.csv");
        }
        if (Sofa.class.equals(furnitureClass)) {
            return (FurnitureDao<F>) new SofaDao("Sofas.csv");
        }
        return null;
    }

}
