package itpu.uz.controller.factory;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Predicate;

public class Console implements Closeable {
    private final BufferedReader reader;

    public Console() {
        this.reader = new BufferedReader(new InputStreamReader(System.in));
    }


    public String readLine() throws IOException {
        return reader.readLine();
    }

    public String readLine(Predicate<String> condition, String errorMessage) throws IOException {
        String line;
        do {
            line = readLine();
            if (condition.test(line)) {
                return line;
            } else {
                error(errorMessage);
            }
        } while (true);
    }


    public void print(String message) {
        System.out.println(message);
    }

    public void error(String message) {
        System.err.println("Error: " + message);
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}