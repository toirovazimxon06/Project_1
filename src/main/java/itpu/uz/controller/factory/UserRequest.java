package itpu.uz.controller.factory;

public enum UserRequest {

    EXIT,
    CANCEL,
    SEARCH,
    COUNT,
    ERROR
}
