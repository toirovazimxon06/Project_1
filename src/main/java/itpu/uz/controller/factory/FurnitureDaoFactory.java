package itpu.uz.controller.factory;

import itpu.uz.dao.FurnitureDao;
import itpu.uz.entity.Furniture;


public interface FurnitureDaoFactory {

    <F extends Furniture<F>> FurnitureDao<F> getFurnitureDAO(Class<F> furnitureClass);

}
