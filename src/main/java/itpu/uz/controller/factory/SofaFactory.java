package itpu.uz.controller.factory;


import itpu.uz.controller.control.SofaCon;
import itpu.uz.controller.parametrs.swap.FurnitureParameterConverter;
import itpu.uz.service.ApplianceService;

import java.util.List;

public class SofaFactory {

    public SofaCon createSofaController(ApplianceService service) {
        return new SofaCon(service, List.of(
                FurnitureParameterConverter.FURNITURE_TYPE.getConverter(),
                FurnitureParameterConverter.FURNITURE_NAME.getConverter(),
                FurnitureParameterConverter.PRICE.getConverter(),
                FurnitureParameterConverter.QUANTITY.getConverter(),
                FurnitureParameterConverter.LENGTH.getConverter(),
                FurnitureParameterConverter.NUMBER_OF_SEATS.getConverter()
        ));
    }

}
