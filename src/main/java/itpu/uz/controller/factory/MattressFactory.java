package itpu.uz.controller.factory;


import itpu.uz.controller.control.MattressCon;
import itpu.uz.controller.parametrs.swap.FurnitureParameterConverter;
import itpu.uz.service.ApplianceService;
import itpu.uz.service.ApplianceServiceImpl;

import java.util.List;

public class MattressFactory {

    public MattressCon createMattressController(ApplianceService service) {
        return new MattressCon(service, List.of(
                FurnitureParameterConverter.FURNITURE_TYPE.getConverter(),
                FurnitureParameterConverter.FURNITURE_NAME.getConverter(),
                FurnitureParameterConverter.PRICE.getConverter(),
                FurnitureParameterConverter.QUANTITY.getConverter(),
                FurnitureParameterConverter.LENGTH.getConverter(),
                FurnitureParameterConverter.COMFORT_LEVEL.getConverter()
        ));
    }

}
