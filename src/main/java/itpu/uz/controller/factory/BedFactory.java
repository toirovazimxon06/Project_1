package itpu.uz.controller.factory;


import itpu.uz.controller.parametrs.swap.FurnitureParameterConverter;
import itpu.uz.service.ApplianceService;
import itpu.uz.service.ApplianceServiceImpl;

import java.util.List;
import itpu.uz.controller.control.BedCon;
public class BedFactory {

    public BedCon createBedController(ApplianceService service) {
        return new BedCon(service, List.of(
                FurnitureParameterConverter.FURNITURE_TYPE.getConverter(),
                FurnitureParameterConverter.FURNITURE_NAME.getConverter(),
                FurnitureParameterConverter.PRICE.getConverter(),
                FurnitureParameterConverter.QUANTITY.getConverter(),
                FurnitureParameterConverter.LENGTH.getConverter(),
                FurnitureParameterConverter.NUMBER_OF_DRAWERS.getConverter()
        ));
    }

}
