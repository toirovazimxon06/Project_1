package itpu.uz.controller.parametrs;

import itpu.uz.entity.Furniture;
import itpu.uz.entity.PowerConsumable;

public class PowerConsumptionParameter<F extends Furniture<F> &
        PowerConsumable<F>> extends AbstractRangeParameter<F, Double> {

    public PowerConsumptionParameter(Double start, Double end) {
        super(start, end);
        if (start < 0) {
            throw new RuntimeException("Power consumption can't be less than 0!");
        }
    }

    @Override
    public boolean isValid(F furniture) {
        return isInRange(furniture.getPowerConsumption());
    }
}
