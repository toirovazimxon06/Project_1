package itpu.uz.controller.parametrs;

import itpu.uz.entity.Mattress;


public record ComfortLevelParameter(String comfortLevel) implements Parameter<Mattress> {

    public ComfortLevelParameter {
        if (comfortLevel == null || comfortLevel.isBlank()) {
            throw new RuntimeException("Comfort level can't be empty!");
        }
    }

    @Override
    public boolean isValid(Mattress comfort) {
        return comfortLevel.equals(comfort.getComfortLevel());
    }
}
