package itpu.uz.controller.parametrs;


import itpu.uz.entity.Furniture;

public abstract class AbstractRangeParameter<F extends Furniture<F>,
        T extends Comparable<T>> implements Parameter<F> {

    private final RangeController<T> rangeController;

    public AbstractRangeParameter(T start, T end) {
        rangeController = new RangeController<>(start, end);
    }

    protected boolean isInRange(T value) {
        return rangeController.isInRange(value);
    }

}
