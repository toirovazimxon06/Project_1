package itpu.uz.controller.parametrs;


import org.apache.commons.lang3.Range;

public class RangeController<T extends Comparable<T>> {
    private final Range<T> range;

    public RangeController(T start, T end) {
        range = Range.between(start, end);
    }

    public boolean isInRange(T value) {
        return range.contains(value);
    }
}
