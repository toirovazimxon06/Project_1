package itpu.uz.controller.parametrs;

import itpu.uz.entity.Table;

public class NumberOfPeopleParameter extends AbstractRangeParameter<Table, Integer> {

    public NumberOfPeopleParameter(Integer start, Integer end) {
        super(start, end);
        if (start < 0) {
            throw new RuntimeException("Number of people can't be less than 0!");
        }
    }

    @Override
    public boolean isValid(Table table) {
        return isInRange(table.getNumberOfPeople());
    }
}
