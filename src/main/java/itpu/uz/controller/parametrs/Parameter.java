package itpu.uz.controller.parametrs;


import itpu.uz.entity.Furniture;

@FunctionalInterface
public interface Parameter<F extends Furniture<F>> {

    boolean isValid(F furniture);

}

