package itpu.uz.controller.parametrs;


import itpu.uz.entity.Furniture;

public record FurnitureTypeParameter<F extends Furniture<F>>(String furnitureType) implements Parameter<F> {

    public FurnitureTypeParameter {
        if (furnitureType == null || furnitureType.isBlank()) {
            throw new RuntimeException("Furniture type can't be empty!");
        }
    }

    @Override
    public boolean isValid(F furniture) {
        return furnitureType.equals(furniture.getFurnitureType());
    }
}
