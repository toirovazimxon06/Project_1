package itpu.uz.controller.parametrs;


import itpu.uz.entity.Furniture;

public class PriceParameter<F extends Furniture<F>> extends AbstractRangeParameter<F, Double> {

    public PriceParameter(Double start, Double end) {
        super(start, end);
        if (start < 0) {
            throw new RuntimeException("Price can't be less than 0!");
        }
    }

    @Override
    public boolean isValid(F furniture) {
        return isInRange(furniture.getPrice());
    }
}
