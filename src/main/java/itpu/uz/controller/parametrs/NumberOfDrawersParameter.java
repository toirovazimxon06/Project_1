package itpu.uz.controller.parametrs;


import itpu.uz.entity.Bed;

public class NumberOfDrawersParameter extends AbstractRangeParameter<Bed, Integer> {

    public NumberOfDrawersParameter(Integer start, Integer end) {
        super(start, end);
        if (start < 0) {
            throw new RuntimeException("Number of drawers can't be less than 0!");
        }
    }

    @Override
    public boolean isValid(Bed bed) {
        return isInRange(bed.getNumberOfDrawers());
    }
}
