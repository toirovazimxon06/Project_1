package itpu.uz.controller.parametrs;


import itpu.uz.entity.Furniture;

public record FurnitureNameParameter<F extends Furniture<F>>(String furnitureName) implements Parameter<F> {

    public FurnitureNameParameter {
        if (furnitureName == null || furnitureName.isBlank()) {
            throw new RuntimeException("Furniture name can't be empty!");
        }
    }

    @Override
    public boolean isValid(F furniture) {
        return furnitureName.equals(furniture.getFurnitureName());
    }
}
