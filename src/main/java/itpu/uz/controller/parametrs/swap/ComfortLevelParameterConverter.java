package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.ComfortLevelParameter;
import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Mattress;

public class ComfortLevelParameterConverter extends AbstractFurnitureParameterConverter<Mattress> {

    public ComfortLevelParameterConverter() {
        super("comfort level");
    }

    @Override
    protected Parameter<Mattress> innerStringConverter(String request) {
        return new ComfortLevelParameter(request);
    }
}
