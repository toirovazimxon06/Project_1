package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.FurnitureNameParameter;
import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Furniture;

public class FurnitureNameParameterConverter<F extends Furniture<F>> extends AbstractFurnitureParameterConverter<F> {

    public FurnitureNameParameterConverter() {
        super("furniture name");
    }

    @Override
    protected Parameter<F> innerStringConverter(String request) {
        return new FurnitureNameParameter<>(request);
    }
}
