package itpu.uz.controller.parametrs.swap;

import itpu.uz.controller.parametrs.NumberOfPeopleParameter;
import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Table;

public class NumberOfPeopleParameterConverter extends AbstractFurnitureParameterConverter<Table> {

    public NumberOfPeopleParameterConverter() {
        super("number of people");
    }

    @Override
    protected Parameter<Table> innerNumericConverter(String[] value) {
        Integer start = Integer.parseInt(value[0].strip());
        Integer end = value.length == 1 ? start : Integer.parseInt(value[1].strip());
        return new NumberOfPeopleParameter(start, end);
    }
}
