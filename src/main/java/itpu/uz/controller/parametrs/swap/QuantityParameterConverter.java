package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.controller.parametrs.QuantityParameter;
import itpu.uz.entity.Furniture;

public class QuantityParameterConverter<F extends Furniture<F>> extends AbstractFurnitureParameterConverter<F> {

    public QuantityParameterConverter() {
        super("quantity");
    }

    @Override
    protected Parameter<F> innerNumericConverter(String[] value) {
        Integer start = Integer.parseInt(value[0].strip());
        Integer end = value.length == 1 ? start : Integer.parseInt(value[1].strip());
        return new QuantityParameter<>(start, end);
    }
}
