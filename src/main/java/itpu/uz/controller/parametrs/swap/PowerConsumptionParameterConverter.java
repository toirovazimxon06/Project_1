package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.controller.parametrs.PowerConsumptionParameter;
import itpu.uz.entity.Furniture;
import itpu.uz.entity.PowerConsumable;

public class PowerConsumptionParameterConverter<F extends Furniture<F> & PowerConsumable<F>>
        extends AbstractFurnitureParameterConverter<F> {

    public PowerConsumptionParameterConverter() {
        super("power consumption");
    }

    @Override
    protected Parameter<F> innerNumericConverter(String[] value) {
        Double start = Double.parseDouble(value[0].strip());
        Double end = value.length == 1 ? start : Double.parseDouble(value[1].strip());
        return new PowerConsumptionParameter<>(start, end);
    }
}
