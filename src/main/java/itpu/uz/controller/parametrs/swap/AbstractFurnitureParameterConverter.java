package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Furniture;

public abstract class AbstractFurnitureParameterConverter<F extends Furniture<F>> implements ParameterConverter<F> {

    private final String parameter;

    protected AbstractFurnitureParameterConverter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public String parameter() {
        return parameter;
    }

    @Override
    public Parameter<F> convertParameter(String request) throws Exception {
        try {
            if (request.contains("-")) {//check if the request is a range
                String[] value = request.split("-", 2);
                return innerNumericConverter(value);
            } else if (request.matches("\\d+(\\.\\d+)?")) {//check if the request is a single number
                String[] value = {request, request};
                return innerNumericConverter(value);
            } else {
                return innerStringConverter(request);
            }
        } catch (NumberFormatException e) {
            throw new Exception("The input value is not a number!");
        } catch (Exception e) {
            throw new Exception("Invalid request!");
        }
    }

    protected Parameter<F> innerStringConverter(String request) throws Exception {
        throw new UnsupportedOperationException();
    }

    protected Parameter<F> innerNumericConverter(String[] value) throws Exception {
        throw new UnsupportedOperationException();
    }

}
