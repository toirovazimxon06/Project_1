package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.FurnitureTypeParameter;
import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Furniture;

public class FurnitureTypeParameterConverter<F extends Furniture<F>> extends AbstractFurnitureParameterConverter<F> {

    public FurnitureTypeParameterConverter() {
        super("furniture type");
    }

    @Override
    protected Parameter<F> innerStringConverter(String request) {
        return new FurnitureTypeParameter<>(request);
    }
}
