package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.LengthParameter;
import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Furniture;

public class LengthParameterConverter<F extends Furniture<F>> extends AbstractFurnitureParameterConverter<F> {

    public LengthParameterConverter() {
        super("length");
    }

    @Override
    protected Parameter<F> innerNumericConverter(String[] value) {
        Double start = Double.parseDouble(value[0].strip());
        Double end = value.length == 1 ? start : Double.parseDouble(value[1].strip());
        return new LengthParameter<>(start, end);
    }
}
