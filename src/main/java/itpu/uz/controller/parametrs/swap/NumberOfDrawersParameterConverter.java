package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.NumberOfDrawersParameter;
import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Bed;

public class NumberOfDrawersParameterConverter extends AbstractFurnitureParameterConverter<Bed> {

    public NumberOfDrawersParameterConverter() {
        super("number of drawers");
    }

    @Override
    protected Parameter<Bed> innerNumericConverter(String[] value) {
        Integer start = Integer.parseInt(value[0].strip());
        Integer end = value.length == 1 ? start : Integer.parseInt(value[1].strip());
        return new NumberOfDrawersParameter(start, end);
    }
}
