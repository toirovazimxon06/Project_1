package itpu.uz.controller.parametrs.swap;


import itpu.uz.entity.Furniture;

@SuppressWarnings("rawtypes")
public enum FurnitureParameterConverter {
    FURNITURE_TYPE(new FurnitureTypeParameterConverter()),
    FURNITURE_NAME(new FurnitureNameParameterConverter()),
    PRICE(new PriceParameterConverter()),
    QUANTITY(new QuantityParameterConverter()),
    LENGTH(new LengthParameterConverter()),
    NUMBER_OF_PEOPLE(new NumberOfPeopleParameterConverter()),
    NUMBER_OF_DRAWERS(new NumberOfDrawersParameterConverter()),
    NUMBER_OF_SEATS(new NumberOfSeatsParameterConverter()),
    COMFORT_LEVEL(new ComfortLevelParameterConverter()),
    POWER_CONSUMPTION(new PowerConsumptionParameterConverter());

    private final ParameterConverter parameterConverter;

    FurnitureParameterConverter(ParameterConverter parameterConverter) {
        this.parameterConverter = parameterConverter;
    }

    @SuppressWarnings("unchecked")
    public <F extends Furniture<F>> ParameterConverter<F> getConverter() {
        return (ParameterConverter<F>) parameterConverter;
    }
}