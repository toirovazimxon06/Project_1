package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.NumberOfSeatsParameter;
import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Sofa;

public class NumberOfSeatsParameterConverter extends AbstractFurnitureParameterConverter<Sofa> {

    public NumberOfSeatsParameterConverter() {
        super("number of seats");
    }

    @Override
    protected Parameter<Sofa> innerNumericConverter(String[] value) {
        Integer start = Integer.parseInt(value[0].strip());
        Integer end = value.length == 1 ? start : Integer.parseInt(value[1].strip());
        return new NumberOfSeatsParameter(start, end);
    }
}
