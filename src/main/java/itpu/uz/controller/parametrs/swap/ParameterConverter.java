package itpu.uz.controller.parametrs.swap;


import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Furniture;

public interface ParameterConverter<F extends Furniture<F>> {

    String parameter();

    Parameter<F> convertParameter(String request) throws Exception;

}
