package itpu.uz.controller.parametrs;


import itpu.uz.entity.Furniture;

public class QuantityParameter<F extends Furniture<F>> extends AbstractRangeParameter<F, Integer> {

    public QuantityParameter(Integer start, Integer end) {
        super(start, end);
        if (start < 0) {
            throw new RuntimeException("Length can't be less than 0!");
        }
    }

    @Override
    public boolean isValid(F furniture) {
        return isInRange(furniture.getQuantity());
    }
}
