package itpu.uz.controller.parametrs;


import itpu.uz.entity.Sofa;

public class NumberOfSeatsParameter extends AbstractRangeParameter<Sofa, Integer> {

    public NumberOfSeatsParameter(Integer start, Integer end) {
        super(start, end);
        if (start < 0) {
            throw new RuntimeException("Number of seats can't be less than 0!");
        }
    }

    @Override
    public boolean isValid(Sofa sofa) {
        return isInRange(sofa.getNumberOfSeats());
    }
}
