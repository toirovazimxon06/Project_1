package itpu.uz.controller.criteria;

import itpu.uz.entity.Bed;

public class BedSearch extends AbstractSearch<Bed> {
    @Override
    public Class<Bed> getFurnitureClass() {
        return Bed.class;
    }
}
