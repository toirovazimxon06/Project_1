package itpu.uz.controller.criteria;


import itpu.uz.entity.Mattress;

public class ComfortSearch extends AbstractSearch<Mattress> {

    @Override
    public Class<Mattress> getFurnitureClass() {
        return Mattress.class;
    }

}
