package itpu.uz.controller.criteria;


import itpu.uz.controller.parametrs.Parameter;
import itpu.uz.entity.Furniture;
import java.util.HashMap;
import java.util.Map;


public abstract class AbstractSearch<F extends Furniture<F>> implements SearchProperty<F> {
    protected final Map<Class<?>, Parameter<F>> parameters = new HashMap<>();

    @Override
    public <E extends Parameter<F>> void addParameter(E parameter) {
        parameters.put(parameter.getClass(), parameter);
    }

    @Override
    public boolean isValid(F furniture) {
        return parameters.values().stream()
                .map(parameter -> parameter.isValid(furniture))
                .reduce(Boolean::logicalAnd)
                .orElse(true);
    }
}
