package itpu.uz.controller.criteria;


import itpu.uz.entity.Furniture;
import itpu.uz.controller.parametrs.Parameter;



public interface SearchProperty<F extends Furniture<F>> {
    boolean isValid(F furniture);

    Class<F> getFurnitureClass();

    <P extends Parameter<F>> void addParameter(P parameter);
}
