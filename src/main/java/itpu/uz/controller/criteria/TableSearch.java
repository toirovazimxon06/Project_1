package itpu.uz.controller.criteria;


import itpu.uz.entity.Table;

public class TableSearch extends AbstractSearch<Table> {
    @Override
    public Class<Table> getFurnitureClass() {
        return Table.class;
    }
}
