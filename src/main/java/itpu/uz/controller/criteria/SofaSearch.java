package itpu.uz.controller.criteria;


import itpu.uz.entity.Sofa;

public class SofaSearch extends AbstractSearch<Sofa> {
    @Override
    public Class<Sofa> getFurnitureClass() {
        return Sofa.class;
    }

}
