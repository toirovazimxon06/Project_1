package itpu.uz.controller.control;



import itpu.uz.controller.parametrs.swap.ParameterConverter;
import itpu.uz.controller.criteria.BedSearch;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.entity.Bed;
import itpu.uz.service.ApplianceService;

import java.util.List;


public class BedCon extends FurnitureCon<Bed> {

    public BedCon(ApplianceService applianceService, List<ParameterConverter<Bed>> converters) {
        super(applianceService, converters);
    }

    @Override
    protected SearchProperty<Bed> createProperty() {
        return new BedSearch();
    }

}
