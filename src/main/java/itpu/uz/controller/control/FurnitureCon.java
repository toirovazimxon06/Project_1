package itpu.uz.controller.control;

import itpu.uz.controller.factory.Console;
import itpu.uz.controller.factory.UserRequest;
import itpu.uz.controller.parametrs.swap.ParameterConverter;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.entity.Furniture;
import itpu.uz.service.ApplianceService;

import java.io.IOException;
import itpu.uz.controller.parametrs.Parameter;

import java.util.*;

import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class FurnitureCon<F extends Furniture<F>> {
    protected final ApplianceService applianceService;
    protected final Map<String, ParameterConverter<F>> parameterConverters;
    private Collection<F> searchResults;

    protected FurnitureCon(ApplianceService applianceService,
                           Collection<ParameterConverter<F>> parameterConverters) {
        this.applianceService = Objects.requireNonNull(applianceService);
        this.parameterConverters = Objects.requireNonNull(parameterConverters).stream()
                .collect(Collectors.toMap(ParameterConverter::parameter, Function.identity()));
    }

      protected abstract SearchProperty<F> createProperty();

    public UserRequest acceptRequest(Console console) throws IOException {
        SearchProperty<F> property = createProperty();
        UserRequest requestStatus = null;

        while (true) {
            String inputRequest = console.readLine();

            switch (inputRequest.trim().toLowerCase()) {
                case "exit":
                    requestStatus = UserRequest.EXIT;
                    break;
                case "search":
                    requestStatus = UserRequest.SEARCH;
                    break;
                case "cancel":
                    requestStatus = UserRequest.CANCEL;
                    break;
                case "count":
                    requestStatus = UserRequest.COUNT;
                    break;
                default:
                    try {
                        property.addParameter(convertToParameter(inputRequest));
                    } catch (IllegalArgumentException e) {
                        console.error("Invalid parameter: " + e.getMessage());
                        // We continue to prompt the user for input until a valid request status is obtained
                        continue;
                    } catch (Exception e) {
                        console.error(e.getMessage());
                        // In case of other exceptions, we log the error and break out of the loop
                        requestStatus = UserRequest.ERROR;
                        break;
                    }
            }
            // Once a user request that affects the loop control is set, we break out of the loop
            if (requestStatus != null) break;
        }

        // Only perform the search if the search or count command was entered
        searchResults = EnumSet.of(UserRequest.SEARCH, UserRequest.COUNT).contains(requestStatus)
                ? applianceService.find(property)
                : Collections.emptyList();

        return requestStatus;
    }



    private Parameter<F> convertToParameter(String parameter) throws Exception {
        String[] values = parameter.split("=");
        ParameterConverter<F> converter = parameterConverters.get(values[0].strip().toLowerCase());
        if (converter == null) {
            throw new IllegalArgumentException(values[0].strip());
        }
        return converter.convertParameter(values[1].strip());
    }

    public void displaySearchResults(Console console) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Available Products");
        searchResults.forEach(product -> console.print(product.toString()));
        System.out.println("You can find products by entering parameters\n1.Name, 2.Price, 3.Quantity, 4.Length:\nChoose: ");

        String searchParameter = scanner.nextLine();
        switch (searchParameter.toLowerCase()) {
            case "1":
            case "name":
                searchAndDisplayResults("Enter Name: ", scanner, product -> product.getFurnitureName());
                break;
            case "2":
            case "price":
                searchAndDisplayResults("Enter Price: ", scanner, product -> String.valueOf(product.getPrice()));
                break;
            case "3":
            case "quantity":
                searchAndDisplayResults("Enter Quantity: ", scanner, product -> String.valueOf(product.getQuantity()));
                break;
            case "4":
            case "length":
                searchAndDisplayResults("Enter Length: ", scanner, product -> String.valueOf(product.getLength()));
                break;
            default:
                System.out.println("Invalid Parameter: " + searchParameter);
                break;
        }
    }

    private void searchAndDisplayResults(String prompt, Scanner scanner, Function<Furniture, String> mapper) {
        System.out.println(prompt);
        String searchValue = scanner.nextLine();
        boolean found = false;

        for (Furniture product : searchResults) {
            if (mapper.apply(product).equalsIgnoreCase(searchValue)||mapper.apply(product).contains(searchValue)) {
                System.out.println(product);
                found = true;
            }
        }

        if (!found) {
            System.out.println("Sorry, no results found for: " + searchValue);
        }
    }


    public void displayCountResults(Console console) {
        console.print(String.valueOf(searchResults.size()));
    }


}
