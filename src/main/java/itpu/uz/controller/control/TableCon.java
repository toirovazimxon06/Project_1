package itpu.uz.controller.control;

import itpu.uz.controller.parametrs.swap.ParameterConverter;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.controller.criteria.TableSearch;
import itpu.uz.entity.Table;
import itpu.uz.service.ApplianceService;

import java.util.List;

public class TableCon extends FurnitureCon<Table> {
    public TableCon(ApplianceService applianceService, List<ParameterConverter<Table>> converters) {
        super(applianceService, converters);
    }

    @Override
    protected SearchProperty<Table> createProperty() {
        return new TableSearch();
    }
}
