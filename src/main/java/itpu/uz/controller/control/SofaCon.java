package itpu.uz.controller.control;
import itpu.uz.controller.parametrs.swap.ParameterConverter;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.controller.criteria.SofaSearch;
import itpu.uz.entity.Sofa;
import itpu.uz.service.ApplianceService;

import java.util.List;

public class SofaCon extends FurnitureCon<Sofa> {

    public SofaCon(ApplianceService applianceService, List<ParameterConverter<Sofa>> converters) {
        super(applianceService, converters);
    }

    @Override
    protected SearchProperty<Sofa> createProperty() {
        return new SofaSearch();
    }

}
