package itpu.uz.controller.control;


import itpu.uz.controller.parametrs.swap.ParameterConverter;
import itpu.uz.controller.criteria.ComfortSearch;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.entity.Mattress;
import itpu.uz.service.ApplianceService;

import java.util.List;

public class MattressCon extends FurnitureCon<Mattress> {

    public MattressCon(ApplianceService applianceService, List<ParameterConverter<Mattress>> converters) {
        super(applianceService, converters);
    }

    @Override
    protected SearchProperty<Mattress> createProperty() {
        return new ComfortSearch();
    }

}
