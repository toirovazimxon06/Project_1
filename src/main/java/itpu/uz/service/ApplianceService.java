package itpu.uz.service;


import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.entity.Furniture;

import java.util.Collection;

public interface ApplianceService {
    <F extends Furniture<F>> Collection<F> find(SearchProperty<F> property);

    static ApplianceService getInstance(){
        return new ApplianceServiceImpl(ServiceFactory.INSTANCE);
    }
}
