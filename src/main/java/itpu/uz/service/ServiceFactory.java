package itpu.uz.service;

import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.controller.factory.FurnitureDaoFactory;
import itpu.uz.controller.factory.MattressFactory;
import itpu.uz.dao.*;
import itpu.uz.entity.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum ServiceFactory implements FurnitureDaoFactory{
    INSTANCE;
    private Map<Class<?>,FurnitureDao> furnitureDaoMap ;
    @Override
    public <F extends Furniture<F>> FurnitureDao<F> getFurnitureDAO(Class<F> furnitureClass) {
        var dao = furnitureDaoMap.get(furnitureClass);
        return dao;
    }

    ServiceFactory() {
        this.furnitureDaoMap = new HashMap<>();
        furnitureDaoMap.put(Bed.class,new BedDao("Beds.csv"));
        furnitureDaoMap.put(Mattress.class,new MattressDao("Mattresses.csv"));
        furnitureDaoMap.put(Sofa.class,new SofaDao("Sofas.csv"));
        furnitureDaoMap.put(Table.class,new TableDao("Tables.csv"));
    }
}
