package itpu.uz.service;

import itpu.uz.controller.factory.FurnitureDaoFactory;
import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.dao.FurnitureDao;
import itpu.uz.entity.Furniture;

import java.util.Collection;

public class ApplianceServiceImpl implements ApplianceService {
    private final FurnitureDaoFactory daoFactory;

    public ApplianceServiceImpl(FurnitureDaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public <F extends Furniture<F>> Collection<F> find(SearchProperty<F> property) {
        FurnitureDao<F> dao = daoFactory.getFurnitureDAO(property.getFurnitureClass());
        if (dao == null) {
            throw new IllegalStateException("DAO is not found!");
        }
        return dao.find(property);
    }
}