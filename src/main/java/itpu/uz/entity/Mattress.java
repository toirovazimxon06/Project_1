package itpu.uz.entity;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;


@EqualsAndHashCode(callSuper = true)
@RequiredArgsConstructor
public final class Mattress extends Furniture<Mattress> {
    private String comfortLevel;

    @Override
    public String toString() {
        return super.toString().replaceAll("}", "") +
                String.join(", ", ", comfort level = " + comfortLevel) + " }";
    }

    public String getComfortLevel() {
        return comfortLevel;
    }

    public void setComfortLevel(String comfortLevel) {
        this.comfortLevel = comfortLevel;
    }
}