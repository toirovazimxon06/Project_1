package itpu.uz.dao;


import itpu.uz.entity.Mattress;

public class MattressDao extends AbstractFurnitureDao<Mattress> {

    public MattressDao(String path) {
        super(path);
    }

    @Override
    protected Mattress parseValues(String[] values) {
        Mattress mattress = new Mattress();
        mattress.setFurnitureType(values[0]);
        mattress.setFurnitureName(values[1]);
        mattress.setPrice(Double.parseDouble(values[2]));
        mattress.setQuantity(Integer.parseInt(values[3]));
        mattress.setLength(Double.parseDouble(values[4]));
        mattress.setComfortLevel(values[5]);
        return mattress;
    }
}