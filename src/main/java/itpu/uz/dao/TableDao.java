package itpu.uz.dao;


import itpu.uz.entity.Table;

public class TableDao extends AbstractFurnitureDao<Table> {

    public TableDao(String path) {
        super(path);
    }

    @Override
    protected Table parseValues(String[] values) {
        Table table = new Table();
        table.setFurnitureType(values[0]);
        table.setFurnitureName(values[1]);
        table.setPrice(Double.parseDouble(values[2]));
        table.setQuantity(Integer.parseInt(values[3]));
        table.setLength(Double.parseDouble(values[4]));
        table.setNumberOfPeople(Integer.parseInt(values[5]));
        return table;
    }
}