package itpu.uz.dao;


import itpu.uz.entity.Bed;

public class BedDao extends AbstractFurnitureDao<Bed> {

    public BedDao(String path) {
        super(path);
    }

    @Override
    public Bed parseValues(String[] values) {
        Bed bed = new Bed();
        bed.setFurnitureType(values[0]);
        bed.setFurnitureName(values[1]);
        bed.setPrice(Double.parseDouble(values[2]));
        bed.setQuantity(Integer.parseInt(values[3]));
        bed.setLength(Double.parseDouble(values[4]));
        bed.setNumberOfDrawers(Integer.parseInt(values[5]));
        return bed;
    }

}