package itpu.uz.dao;

import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.entity.Furniture;

import java.util.Collection;

public interface FurnitureDao<F extends Furniture<F>> {

    Collection<F> find(SearchProperty<F> property);

}
