package itpu.uz.dao;


import itpu.uz.controller.criteria.SearchProperty;
import itpu.uz.entity.Furniture;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractFurnitureDao<F extends Furniture<F>> implements FurnitureDao<F> {

    private final Path filePath;

    protected AbstractFurnitureDao(String filePath) {
        try {
            URL url = getClass().getClassLoader().getResource(filePath);
            if (url == null) {
                throw new IllegalArgumentException("CSV file is not found!");
            }
            this.filePath = Path.of(url.toURI());
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Collection<F> find(SearchProperty<F> property) {
        try (BufferedReader br = Files.newBufferedReader(filePath)) {
            br.readLine(); // skip header line
            String line;
            ArrayList<F> furnitureList = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                F furniture = parseValues(values);
                if (property.isValid(furniture)) furnitureList.add(furniture);
            }
            return furnitureList;
        } catch (IOException e) {
            return List.of();
        }
    }

    protected abstract F parseValues(String[] values);

}