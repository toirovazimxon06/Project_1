package itpu.uz.dao;


import itpu.uz.entity.Sofa;

public class SofaDao extends AbstractFurnitureDao<Sofa> {

    public SofaDao(String path) {
        super(path);
    }

    @Override
    protected Sofa parseValues(String[] values) {
        Sofa sofa = new Sofa();
        sofa.setFurnitureType(values[0]);
        sofa.setFurnitureName(values[1]);
        sofa.setPrice(Double.parseDouble(values[2]));
        sofa.setQuantity(Integer.parseInt(values[3]));
        sofa.setLength(Double.parseDouble(values[4]));
        sofa.setNumberOfSeats(Integer.parseInt(values[5]));
        return sofa;
    }
}