package itpu.uz.view;

public class View {

    public void displayWelcomeMessage() {
        System.out.println("\nWelcome to the Warehouse Searching System!");
        displayVersionInfo();
    }

    public void displayMainMenu() {
        System.out.println("********************************************************************************");
        System.out.println("*                                                                              *");
        System.out.println("*              Greetings and Welcome to the Warehouse Search System!           *");
        System.out.println("*                                 Furniture Division                           *");
        System.out.println("********************************************************************************");
        System.out.println();
        System.out.println("Primary Menu:");
        System.out.println("Inventory Categories: beds, mattresses, sofas, tables");
        System.out.println("Operational Commands: search, count, exit");
        System.out.println("Type 'info' to receive more details on system usage");
        System.out.println("Type 'exit' to terminate the program session");
        System.out.println("Please Indicate Furniture Category:");
        System.out.println("beds, mattresses, sofas, tables");
        System.out.println("********************************************************************************");
        System.out.println("Input the furniture category for inventory lookup or a command to continue:");


    }

    public void displayInfoMessage() {
        System.out.println("\nWarehouse Search System is written in Java");
        displayVersionInfo();
    }

    public void displayVersionInfo() {
        System.out.println("\n\tWarehouse Search System - Furniture Warehouse");
        System.out.println("\t\tVersion: Java 17");
        System.out.println("\t\tDate: 4/25/2024");
        System.out.println("Student email: azimkhon_toirov@student.itpu.uz");
    }

    public void displayGoodbyeMessage() {
        System.out.println("\nThank you. Goodbye!");
    }
    public void use(){
        System.out.println("Write ----- >Available Furniture: {beds, mattresses, sofas, tables} ---->Then Write --->Available Commands: {search, count, cancel}");
        System.out.println("Type 'exit' to close the program");
    }
}
