package itpu.uz.view;

import itpu.uz.controller.control.FurnitureCon;
import itpu.uz.controller.factory.Console;
import itpu.uz.controller.factory.UserRequest;
import itpu.uz.entity.Furniture;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;

public class WarehouseView implements Closeable {
    private final Console console;
    private final Map<String, FurnitureCon<?>> furnitureControllers;
    private final View view;

    public WarehouseView(Map<String, FurnitureCon<?>> furnitureControllers, View view) {
        this.console = new Console();
        this.furnitureControllers = Objects.requireNonNull(furnitureControllers);
        this.view = Objects.requireNonNull(view);
    }

    public void processRequests() {
        final String EXIT_COMMAND = "exit";
        final String INFO_COMMAND = "info";

        printTitle();
        view.displayWelcomeMessage();
        view.displayMainMenu();

        try (Console console = new Console()) {
            String request;
            while (!(request = promptAndRead(console, "Enter Furniture Typen{beds, mattresses, sofas, tables}n")).equalsIgnoreCase(EXIT_COMMAND)) {
                if (request.equalsIgnoreCase(INFO_COMMAND)) {
                    view.displayInfoMessage();
                    continue;
                }
                processFurnitureRequest(console, request.toLowerCase());
            }
        } catch (IOException e) {
            console.error("An error occurred: " + e.getMessage());
        }

        view.displayGoodbyeMessage();
    }

    private String promptAndRead(Console console, String prompt) throws IOException {
        System.out.println(prompt);
        return console.readLine();
    }

    private void processFurnitureRequest(Console console, String furnitureType) throws IOException {
        FurnitureCon<? extends Furniture<?>> controller = furnitureControllers.get(furnitureType);
        if (controller == null) {
            console.error("Unsupported furniture type: " + furnitureType);
            view.use();
            return;
        }

        System.out.println("Enter Commandn{search, count, exit}");
        UserRequest status = controller.acceptRequest(console);
        if (status == UserRequest.SEARCH) {
            controller.displaySearchResults(console);
        } else if (status == UserRequest.COUNT) {
            controller.displayCountResults(console);
        }
    }

    @Override
    public void close() throws IOException {
        console.close();
    }
    public  void printTitle() {
        try {
            Path path = Paths.get("D:\\Java\\Project_1\\src\\main\\resources\\font");
            var str = new String(Files.readAllBytes(path));
            System.out.println(str);
        } catch (Exception ignored) {

        }
    }
}
