package itpu.uz.main.console;

import itpu.uz.view.WarehouseView;
import itpu.uz.controller.factory.ControllerFactory;
import itpu.uz.service.ApplianceService;

public class Main {
    public static void main(String[] args){
        ApplianceService service = ApplianceService.getInstance();
        ControllerFactory factory = new ControllerFactory();
        try (WarehouseView controller = factory.createController(service)) {
            controller.processRequests();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
