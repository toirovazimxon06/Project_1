# Warehouse Furniture Project

## Overview
The Warehouse Furniture Project is a robust and scalable Java application designed to manage the inventory of a furniture warehouse. It facilitates tracking furniture items, managing stock levels, and handling orders. Built with industry standards, our goal is to deliver an efficient and user-friendly system to enhance warehouse management.

## Features
- Inventory Management: Add, update, delete, and search for furniture items in the inventory.
- Stock Tracking: Monitor stock levels with automatic updates upon purchases and sales.
- Order Processing: Process orders, including sales, returns, and cancellations.
- Reporting: Generate detailed reports for inventory, sales, and order history.
- User Interface: Easy-to-use GUI for seamless navigation and management of warehouse operations.

## Getting Started
To get the Warehouse Furniture Project up and running on your local machine, follow these steps:

#### Prerequisites
- Java JDK 8 or later
- MySQL Server 5.7 or later (or another compatible database system)
- Git (for version control)

#### Installation
1. Clone the repository to your local machine:


2. Navigate to the project directory:

   cd warehouse-furniture-project

3. Compile the project using your preferred Java build tool (e.g., Maven, Gradle).

4. Configure your database connection settings in src/main/resources/database.properties.

5. Initialize the database using the provided schema found in src/main/resources/schema.sql.

6. Run the application:

   java -jar warehouse-furniture-project.jar


## Usage
Run the warehouse application and log in with your user credentials. The main dashboard provides access to various modules:

- Inventory: Manage your furniture items catalog.
- Orders: Create and process orders.
- Reports: View and export relevant reports.

## Structure
The codebase is organized as follows:
- src/main/java/com/warehouse/model - Contains the data models.
- src/main/java/com/warehouse/service - Business logic for handling data operations.
- src/main/java/com/warehouse/controller - Connects the UI with the service layer.
- src/main/java/com/warehouse/ui - User interface files.

## Contributing
We welcome contributions! Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

## Authors

## License

## Acknowledgments
- Special thanks to contributors and supporters of this project.

---
Here is the video in a resources -----  Screen recording 3 (online-video-cutter.com).mp4